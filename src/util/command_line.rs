use std::rc::Rc;

use crate::database::{container, user::*};
use crate::util::dbhandler::*;
use crate::Application;
use sqlite::Result;
use tui::{backend::Backend, layout::Rect, terminal::Frame};

use unicode_width::UnicodeWidthStr;

const MISSING_PARAMETERS: &str = "You are missing parameters";

pub fn command_line(app: &mut Application::App, db: &Database, user: &User) -> Result<()> {
	if app.selected_tab == 0 {
		home_commands(app, db, user)?;
	}
	if app.selected_tab == 1 {
		env_variable_commands(app, db, user)?;
	}
	Result::Ok(())
}

pub fn home_commands(app: &mut Application::App, db: &Database, user: &User) -> Result<()> {
	let v: Vec<&str> = app.input_command.split_whitespace().collect();
	if let Some((&command, args)) = v.split_first() {
		match command {
			"add" => {
				for (i, &arg) in args.iter().enumerate() {
					match arg {
						"workspace" => {
							if check_arguments(args, 2) == true {
								let name = args[i + 1];
								container::create_workspace(user, name, db)?;
							} else {
								println!("{}", MISSING_PARAMETERS);
							}
						}
						"collection" => {
							if check_arguments(args, 2) == true {
								let name = args[i + 1];
								container::create_collection(
									name,
									app.workspace_state.selected().unwrap() as i64 + 1,
									db,
								)?;
							} else {
								println!("{}", MISSING_PARAMETERS);
							}
						}
						"request" => {
							if check_arguments(args, 4) == true {
								let name = args[i + 1];
								let id = app.collections[app.col_state.selected().unwrap()].id;
								let method = args[i + 2];
								let url = args[i + 3];
								container::create_request(name, id, method, url, db)?;
							} else {
								println!("{}", MISSING_PARAMETERS);
							}
						}
						&_ => (),
					}
				}
			}
			"delete" => {
				for (_i, &arg) in args.iter().enumerate() {
					match arg {
						"workspace" => {
							let id: i64 =
								app.workspaces[app.workspace_state.selected().unwrap()].id;
							container::delete_workspace(id, db)?;
						}
						&_ => (),
					}
				}
			}
			"help" => {
				app.input_mode = Application::InputMode::HelpCommands;
			}
			&_ => println!("command {} not found", command),
		}
		app.input_command.drain(..);
		app.input_mode = Application::InputMode::Normal;
		Result::Ok(())
	} else if v.split_first().is_none() {
		app.input_command.drain(..);
		app.input_mode = Application::InputMode::Normal;
		Result::Ok(())
	} else {
		Result::Err(sqlite::Error {
			code: Some(500),
			message: Some("Couldn't access db".to_string()),
		})
	}
}

pub fn env_variable_commands(app: &mut Application::App, db: &Database, user: &User) -> Result<()> {
	let v: Vec<&str> = app.input_command.split_whitespace().collect();
	if let Some((&name, args)) = v.split_first() {
		match name {
			"add" => {
				for (i, &arg) in args.iter().enumerate() {
					match arg {
						"variable" => {
							if check_arguments(args, 3) == true {
								let key = args[i + 1];
								container::create_variable_key(key, 1, 1, db)?;
							} else {
								println!("{}", MISSING_PARAMETERS);
							}
						}
						"environnment" => {
							if check_arguments(args, 2) == true {
								let name = args[i + 1];
								container::create_environment(name, db)?;
							} else {
								println!("{}", MISSING_PARAMETERS);
							}
						}
						&_ => (),
					}
				}
			}
			"delete" => {
				for (_i, &arg) in args.iter().enumerate() {
					match arg {
						"workspace" => {
							let id: i64 =
								app.workspaces[app.workspace_state.selected().unwrap()].id;
							container::delete_workspace(id, db)?;
						}
						&_ => (),
					}
				}
			}
			"help" => {
				app.input_mode = Application::InputMode::HelpCommands;
			}
			&_ => println!("command {} not found", name),
		}
		app.input_command.drain(..);
		app.input_mode = Application::InputMode::Normal;
		Result::Ok(())
	} else if v.split_first().is_none() {
		Result::Ok(())
	} else {
		Result::Err(sqlite::Error {
			code: Some(500),
			message: Some("Couldn't access db".to_string()),
		})
	}
}

pub fn update_cursor_pos<B: Backend>(
	f: &mut Frame<B>,
	app: &mut Application::App,
	input_chunk: &Rc<[Rect]>,
) {
	f.set_cursor(
		// Place the cursor at the end of the input as you are
		// typing.
		input_chunk[0].x + app.input_command.width() as u16 + 1,
		// Move one line down to leave a border.
		input_chunk[0].y + 1,
	)
}

fn check_arguments(args: &[&str], num: usize) -> bool {
	let mut proceed: bool = true;
	if args.len() != num {
		proceed = false;
	}
	proceed
}
