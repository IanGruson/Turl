use std::collections::HashMap;
use std::error::Error;
use tui::{
	backend::Backend,
	layout::{Constraint, Direction, Layout},
	style::{Color, Modifier, Style},
	symbols::DOT,
	text::{Span, Line},
	widgets::{Block, Borders, Clear, List, Paragraph, Tabs},
	Frame, Terminal,
};
use unicode_width::UnicodeWidthStr;

use crate::util::{command_line, dbhandler::*};
use crate::{database::container::*, ui::view, Application};

pub fn render_home_screen<B: Backend>(f: &mut Frame<B>, app: &mut Application::App, db: &Database) -> Result<(), Box<dyn Error>> {
	let chunks = Layout::default()
		.direction(Direction::Vertical)
		.margin(0)
		.constraints(
			[
				Constraint::Percentage(5),
				Constraint::Percentage(85),
				Constraint::Percentage(6),
				Constraint::Percentage(4),
			]
			.as_ref(),
		)
		.split(f.size());

	//--------WORKSPACES--------
	app.workspaces = get_all_workspaces(1, db).unwrap();

	if app.workspaces.len() == 0 {
		app.workspaces
			.push(Workspace::new(0, String::from("add a Workspace")));
	}
	app.tab_len = app.workspaces.len();

	app.workspaces_line = view::container_to_spans(app.workspaces.clone());

	// tabs for Workspaces
	let tabs = Tabs::new(app.workspaces_line.clone())
		.block(Block::default().title("Workspaces"))
		.highlight_style(Style::default().fg(Color::Black).bg(Color::White))
		.select(app.workspace_state.selected().unwrap())
		.divider(DOT);
	f.render_widget(tabs, chunks[0]);

	let horizontal_chunks = Layout::default()
		.direction(Direction::Horizontal)
		.margin(0)
		.constraints([Constraint::Percentage(15), Constraint::Percentage(85)].as_ref())
		.split(chunks[1]);

	let left_bar_chunks = Layout::default()
		.direction(Direction::Vertical)
		.constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
		.split(horizontal_chunks[0]);

	//--------COLLECTIONS--------
	let mut collections =
		get_all_collections(app.workspace_state.selected().unwrap() as i64 + 1, db).unwrap();
	app.collections = collections.clone();
	if collections.len() < 1 {
		collections.push(Collection::new(0, String::from("Empty")));
	}
	let collection_items = view::container_to_ListItem(collections);
	app.collection_list = collection_items.clone();

	// Render the collections of a workspace in a Widget::List
	let collection_list = List::new(app.collection_list.clone())
		.block(Block::default().title("Collections").borders(Borders::ALL))
		.highlight_style(Style::default().add_modifier(Modifier::ITALIC))
		.highlight_symbol(">>");
	f.render_stateful_widget(collection_list, left_bar_chunks[0], &mut app.col_state);

	if app.collections.len() < 1 {
		app.collections
			.push(Collection::new(0, String::from("Empty")));
	}
	let id_selected_col = app.collections[app.col_state.selected().unwrap()].id;

	//--------REQUESTS--------
	let requests = get_all_requests(id_selected_col, db).unwrap();
	app.requests = requests.clone();

	let request_items = view::element_to_ListItem(requests);
	app.request_list = request_items;
	let request_list = List::new(app.request_list.clone())
		.block(Block::default().title("Requests").borders(Borders::ALL))
		.highlight_style(Style::default().add_modifier(Modifier::ITALIC))
		.highlight_symbol(">>");
	f.render_stateful_widget(request_list, left_bar_chunks[1], &mut app.req_state);

	// Checks if app.requests are empty; if so add a "default" requests.
	if app.requests.len() == 0 {
		app.requests.push(Request {
			id: 0,
			name: String::from("add Request"),
			method: Methods::GET,
			url: String::from("This collection is empty, add a request !"),
			params: String::from(" "),
			body: String::from(" "),
		});
	}

	let request = &app.requests[app.req_state.selected().unwrap()];

	let request_paragraph = Paragraph::new(vec![Line::from(vec![
		Span::styled(
			request.method.to_string(),
			Style::default().add_modifier(Modifier::ITALIC),
		),
		Span::styled("   ", Style::default()),
		Span::styled(request.url.clone(), Style::default()),
	])])
	.block(Block::default().title("Edit Request").borders(Borders::ALL));
	f.render_widget(request_paragraph, horizontal_chunks[1]);

	let request_chunks = Layout::default()
		.direction(Direction::Vertical)
		.margin(1)
		.constraints([Constraint::Percentage(20), Constraint::Percentage(80)])
		.split(horizontal_chunks[1]);

	//--------RESPONSE--------
	if app.response.len() == 0 {
		app.response
			.push(String::from("Press enter to run request"));
	}
	let response_paragraph = Paragraph::new(app.response[0].clone())
		.block(Block::default().title("Response").borders(Borders::TOP))
		.scroll((app.response_scroll, 0));
	f.render_widget(response_paragraph, request_chunks[1]);

	let input_chunk = Layout::default()
		.direction(Direction::Vertical)
		.margin(0)
		.constraints([Constraint::Percentage(100)].as_ref())
		.split(chunks[2]);

	let input = Paragraph::new(app.input_command.to_string())
		.style(match app.input_mode {
			Application::InputMode::Normal => Style::default(),
			Application::InputMode::Command => Style::default(),
			Application::InputMode::Editing => Style::default().fg(Color::Yellow),
			Application::InputMode::HelpControls => Style::default(),
			Application::InputMode::HelpCommands => Style::default(),
		})
		.block(Block::default().borders(Borders::ALL).title("Input"));
	f.render_widget(input, input_chunk[0]);

	let contrast_style = Style::default().bg(Color::White).fg(Color::Black);

	let mut controls_map: HashMap<String, String> = HashMap::new();
	controls_map.insert(String::from("?"), String::from("Help"));
	controls_map.insert(String::from("c"), String::from("Input commands"));
	controls_map.insert(String::from("q"), String::from("Quit"));
	Application::render_control_footer(f, &chunks, controls_map, 3, app);

	//Move cursor to the bottom of the page.
	match app.input_mode {
		Application::InputMode::Command => {
			command_line::update_cursor_pos(f, app, &input_chunk);
		}
		//--------HELP CONTROLS PROMPT--------
		Application::InputMode::HelpControls => {
			let spans_vec = vec![
                                          Line::from(Span::styled("?           -- Open the help prompt", Style::default())),
                                          Line::from(Span::styled(":           -- Enter command line mode", Style::default())),
                                          Line::from(Span::styled("q           -- Quit the application", Style::default())),
                                          Line::from(Span::styled("h           -- Move to the Workspace to the left", Style::default())),
                                          Line::from(Span::styled("l           -- Move to the Workspace to the right", Style::default())),
                                          Line::from(Span::styled("j           -- Move down", Style::default())),
                                          Line::from(Span::styled("k           -- Move up", Style::default())),
                                          Line::from(Span::styled("r           -- Enter request selection mode", Style::default())),
                                          Line::from(Span::styled("Space       -- Switch form Collection seletion to request selection (vice versa)", Style::default())),
                                          Line::from(Span::styled("Enter       -- Execute the currently selected request", Style::default())),
                                          Line::from(Span::styled("Backspace   -- Delete the currently selected request", Style::default())),
            ];

			Application::render_help_box(f, spans_vec, 60, 40);
		}
		//--------HELP COMMANDS PROMPT--------
		Application::InputMode::HelpCommands => {
			let spans_vec = vec![
				Line::from(Span::styled(
					"To enter these commands, enter command mode by hitting ':' ",
					Style::default(),
				)),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("--------Request--------", Style::default())),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled(
					"add request [NAME] [METHOD] [URL]",
					Style::default(),
				)),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("--------Collection--------", Style::default())),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("add collection [NAME]", Style::default())),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("--------Workspace--------", Style::default())),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("add workspace [NAME]", Style::default())),
			];

			Application::render_help_box(f, spans_vec, 60, 40);
		}
		_ => {}
	};
	Ok(())
}
