use crate::{
	database::container::Variable,
	util::{
		command_line,
		environment_variables::{self, save_variable, get_key_from_list_item}, dbhandler::Database,
	},
	Application::{self, InputMode, KeyQuery},
};
use std::{error::Error, rc::Rc};
use termion::event::Key;
use tui::{
	backend::Backend,
	layout::{Constraint, Direction, Layout, Rect},
	style::{Color, Modifier, Style},
	terminal::Frame,
	text::{Span, Line},
	widgets::Paragraph,
	widgets::{Block, Borders, List, ListItem},
};

use std::collections::HashMap;

pub fn render_env_var_screen<B: Backend>(
	f: &mut Frame<B>,
	app: &mut Application::App,
	db: &Database
) -> Result<(), Box<dyn Error>> {
	let chunks = Layout::default()
		.direction(Direction::Vertical)
		.margin(0)
		.constraints(
			[
				Constraint::Percentage(5),
				Constraint::Percentage(85),
				Constraint::Percentage(6),
				Constraint::Percentage(4),
			]
			.as_ref(),
		)
		.split(f.size());

	if app.variables_list.is_empty() {
		let mut span;
		let mut list_item;
		for (key, value) in app.variables_map.variables_map.iter() {

			span = Span::styled(
				format!("{} = {}", key, value.1),
				Style::default().fg(Color::Yellow),
			);
			list_item = ListItem::new(span);
			app.variables_list.push(list_item);
		}
		span = Span::styled(format!("+ add new"), Style::default().fg(Color::Yellow));
		list_item = ListItem::new(span);
		app.variables_list.push(list_item);
	}
	if app.input_mode.eq(&InputMode::Editing) {
		display_var_editing(app);
	}

	let variable_text = List::new(app.variables_list.clone())
		.block(
			Block::default()
				.title("Environmnent variables")
				.borders(Borders::ALL),
		)
		.highlight_style(
			Style::default()
				.fg(Color::Black)
				.bg(Color::Yellow)
				.add_modifier(Modifier::SLOW_BLINK),
		);
	f.render_stateful_widget(variable_text, chunks[1], &mut app.variable_state);
	match_input(f, app, &chunks)?;
	editing_input_match(app)?;

	let new_var_key: &String = &app.environment_kv_input.0;
	let new_var_value: &String = &app.environment_kv_input.1;

	match app.var_editing_mode {
		Application::VarEditingMode::Validating => {
			if !new_var_key.is_empty() && !new_var_value.is_empty() {
				let idx = app.variable_state.selected().unwrap();
				let id = app.variables_map.get(&KeyQuery::Index(idx)).unwrap().0;
				// dbg!(&app.variables_map);

				save_variable(
					db,
					Variable {
						id,
						key: (*new_var_key.clone()).to_string(),
						value: (*new_var_value.clone()).to_string(),
					},
				)?;
			}
			app.environment_kv_input = (String::from(""), String::from(""));
			app.var_editing_mode = Application::VarEditingMode::None;
		}
		_ => {}
	}

	render_input_box(f, app, &chunks);
	let mut controls_map: HashMap<String, String> = HashMap::new();
	controls_map.insert(String::from("?"), String::from("Help"));
	controls_map.insert(
		String::from("c"),
		String::from("variable management commands"),
	);
	controls_map.insert(String::from("q"), String::from("Quit"));
	Application::render_control_footer(f, &chunks, controls_map, 3, app);
	Ok(())
}

pub fn render_input_box<B: Backend>(
	f: &mut Frame<B>,
	app: &mut Application::App,
	chunks: &Rc<[Rect]>,
) {
	let input_chunk = Layout::default()
		.direction(Direction::Vertical)
		.margin(0)
		.constraints([Constraint::Percentage(100)].as_ref())
		.split(chunks[2]);

	let input = Paragraph::new(app.input_command.to_string())
		.style(match app.input_mode {
			_ => Style::default(),
		})
		.block(Block::default().borders(Borders::ALL).title("Input"));
	f.render_widget(input, input_chunk[0]);
}

fn match_input<B: Backend>(
	f: &mut Frame<B>,
	app: &mut Application::App,
	chunks: &Rc<[Rect]>,
) -> Result<(), Box<dyn Error>> {
	match app.input_mode {
		Application::InputMode::Command => {
			command_line::update_cursor_pos(f, app, chunks);
		}
		Application::InputMode::Editing => {
			let idx = app.variable_state.selected().unwrap();

			app.variables_list[idx] = ListItem::new(format!("{} = {}", app.environment_kv_input.0, app.environment_kv_input.1));
			environment_variables::cursor_for_env(
				f,
				chunks[1],
				app.variables_list[idx].clone(),
				app.variable_state.selected().unwrap() as u16,
				app,
			);
		}
		Application::InputMode::HelpCommands => {
			let spans_vec = vec![
				Line::from(Span::styled(
					"To enter these commands, enter command mode by hitting ':' ",
					Style::default(),
				)),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("--------Request--------", Style::default())),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled(
					"add env [NAME] [METHOD] [URL]",
					Style::default(),
				)),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("--------Collection--------", Style::default())),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("add collection [NAME]", Style::default())),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("--------Workspace--------", Style::default())),
				Line::from(Span::styled(" ", Style::default())),
				Line::from(Span::styled("add workspace [NAME]", Style::default())),
			];

			Application::render_help_box(f, spans_vec, 60, 40);
		}

		_ => {}
	}
	Ok(())
}

fn editing_input_match(app: &mut Application::App) -> Result<(), Box<dyn Error>> {
	match app.input_mode {
		Application::InputMode::Editing => match app.event_input {
			Key::Char('\n') => match app.var_editing_mode {
				Application::VarEditingMode::None => {
					app.var_editing_mode = Application::VarEditingMode::Key;
					app.event_input = Key::Null;
				}
				Application::VarEditingMode::Key => {
					app.var_editing_mode = Application::VarEditingMode::Value;
					app.event_input = Key::Null;
				}
				Application::VarEditingMode::Value => {
					app.input_mode = Application::InputMode::Normal;
					app.var_editing_mode = Application::VarEditingMode::Validating;
				}
				_ => {}
			},
			Key::Char(c) => match app.var_editing_mode {
				Application::VarEditingMode::Key => {
					app.environment_kv_input.0.push(c);
					app.event_input = Key::Null;
				}
				Application::VarEditingMode::Value => {
					app.environment_kv_input.1.push(c);
					app.event_input = Key::Null;
				}
				_ => {}
			},
			Key::Backspace => match app.var_editing_mode {
				Application::VarEditingMode::Key => {
					app.environment_kv_input.0.pop();
				}
				Application::VarEditingMode::Value => {
					app.environment_kv_input.1.pop();
				}
				_ => {}
			},
			_ => {}
		},
		_ => {}
	}
	Ok(())
}

fn display_var_editing(app: &mut Application::App) {

	let span;
	let list_item;
	let formatted_list_item = match app.var_editing_mode {
		Application::VarEditingMode::Key => { format!("Key = {} ", app.environment_kv_input.0)},
		Application::VarEditingMode::Value => { format!("Value = {} ", app.environment_kv_input.1)},
		_ => {format!("") }
	};

	span = Span::styled(
		formatted_list_item,
		Style::default().fg(Color::Yellow),
	);
	list_item = ListItem::new(span);
	let idx = app.variable_state.selected().unwrap();
	app.variables_list[idx] = list_item;
		
}
